﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Kontrakt
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class MatematykSerwis : IMatematyk
    {
        public double Kwadrat(double n)
        {
            double result = n * n;
            Console.WriteLine(n + "^2 = " + result);
            return result;
        }

        public double Pierwiastek(double n)
        {
            double result = Math.Sqrt(n);
            Console.WriteLine("sqrt("+n + ") = " + result);
            return result;
        }

        public double Suma(double a, double b)
        {
            double result = a + b;
            Console.WriteLine(a +" + "+b+" = " + result);
            return result;
        }

        public long Silnia(long n)
        {
            long result = SilniaRec(n);
            Console.WriteLine(n + "! = " + result);
            return result;
        }

        private long SilniaRec(long n)
        {
            if (n == 0) return 1;
            return n * SilniaRec(n - 1);
        }
        
    }

    public class PolonistaSerwis : IPolonista
    {
        public string ToLower(string napis)
        {
            string result = napis.ToLower();
            Console.WriteLine(napis + " toLower is: " + result);
            return result;
        }

        public string ToUpper(string napis)
        {
            string result = napis.ToUpper();
            Console.WriteLine(napis + " toUpper is: " + result);
            return result;
        }
    }
}
