﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Klient.MatematykSerwisRef {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MatematykSerwisRef.IMatematyk")]
    public interface IMatematyk {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Silnia", ReplyAction="http://tempuri.org/IMatematyk/SilniaResponse")]
        long Silnia(long n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Silnia", ReplyAction="http://tempuri.org/IMatematyk/SilniaResponse")]
        System.Threading.Tasks.Task<long> SilniaAsync(long n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Kwadrat", ReplyAction="http://tempuri.org/IMatematyk/KwadratResponse")]
        double Kwadrat(double n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Kwadrat", ReplyAction="http://tempuri.org/IMatematyk/KwadratResponse")]
        System.Threading.Tasks.Task<double> KwadratAsync(double n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Pierwiastek", ReplyAction="http://tempuri.org/IMatematyk/PierwiastekResponse")]
        double Pierwiastek(double n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Pierwiastek", ReplyAction="http://tempuri.org/IMatematyk/PierwiastekResponse")]
        System.Threading.Tasks.Task<double> PierwiastekAsync(double n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Suma", ReplyAction="http://tempuri.org/IMatematyk/SumaResponse")]
        double Suma(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMatematyk/Suma", ReplyAction="http://tempuri.org/IMatematyk/SumaResponse")]
        System.Threading.Tasks.Task<double> SumaAsync(double a, double b);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMatematykChannel : Klient.MatematykSerwisRef.IMatematyk, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MatematykClient : System.ServiceModel.ClientBase<Klient.MatematykSerwisRef.IMatematyk>, Klient.MatematykSerwisRef.IMatematyk {
        
        public MatematykClient() {
        }
        
        public MatematykClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MatematykClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MatematykClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MatematykClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public long Silnia(long n) {
            return base.Channel.Silnia(n);
        }
        
        public System.Threading.Tasks.Task<long> SilniaAsync(long n) {
            return base.Channel.SilniaAsync(n);
        }
        
        public double Kwadrat(double n) {
            return base.Channel.Kwadrat(n);
        }
        
        public System.Threading.Tasks.Task<double> KwadratAsync(double n) {
            return base.Channel.KwadratAsync(n);
        }
        
        public double Pierwiastek(double n) {
            return base.Channel.Pierwiastek(n);
        }
        
        public System.Threading.Tasks.Task<double> PierwiastekAsync(double n) {
            return base.Channel.PierwiastekAsync(n);
        }
        
        public double Suma(double a, double b) {
            return base.Channel.Suma(a, b);
        }
        
        public System.Threading.Tasks.Task<double> SumaAsync(double a, double b) {
            return base.Channel.SumaAsync(a, b);
        }
    }
}
