﻿namespace Klient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KwadratBtn = new System.Windows.Forms.Button();
            this.SqrtBtn = new System.Windows.Forms.Button();
            this.SilniaBtn = new System.Windows.Forms.Button();
            this.ToLowerBtn = new System.Windows.Forms.Button();
            this.ToUpperBtn = new System.Windows.Forms.Button();
            this.Wynik = new System.Windows.Forms.Label();
            this.InputTextBox = new System.Windows.Forms.TextBox();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.txtSecondNr = new System.Windows.Forms.TextBox();
            this.btnSum = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // KwadratBtn
            // 
            this.KwadratBtn.Location = new System.Drawing.Point(12, 86);
            this.KwadratBtn.Name = "KwadratBtn";
            this.KwadratBtn.Size = new System.Drawing.Size(104, 27);
            this.KwadratBtn.TabIndex = 0;
            this.KwadratBtn.Text = "Kwadrat";
            this.KwadratBtn.UseVisualStyleBackColor = true;
            this.KwadratBtn.Click += new System.EventHandler(this.KwadratBtn_Click);
            // 
            // SqrtBtn
            // 
            this.SqrtBtn.Location = new System.Drawing.Point(135, 86);
            this.SqrtBtn.Name = "SqrtBtn";
            this.SqrtBtn.Size = new System.Drawing.Size(99, 27);
            this.SqrtBtn.TabIndex = 1;
            this.SqrtBtn.Text = "Pierwiastek";
            this.SqrtBtn.UseVisualStyleBackColor = true;
            this.SqrtBtn.Click += new System.EventHandler(this.SqrtBtn_Click);
            // 
            // SilniaBtn
            // 
            this.SilniaBtn.Location = new System.Drawing.Point(252, 86);
            this.SilniaBtn.Name = "SilniaBtn";
            this.SilniaBtn.Size = new System.Drawing.Size(102, 27);
            this.SilniaBtn.TabIndex = 2;
            this.SilniaBtn.Text = "Silnia";
            this.SilniaBtn.UseVisualStyleBackColor = true;
            this.SilniaBtn.Click += new System.EventHandler(this.SilniaBtn_Click);
            // 
            // ToLowerBtn
            // 
            this.ToLowerBtn.Location = new System.Drawing.Point(12, 174);
            this.ToLowerBtn.Name = "ToLowerBtn";
            this.ToLowerBtn.Size = new System.Drawing.Size(158, 27);
            this.ToLowerBtn.TabIndex = 3;
            this.ToLowerBtn.Text = "Zmień na małe litery";
            this.ToLowerBtn.UseVisualStyleBackColor = true;
            this.ToLowerBtn.Click += new System.EventHandler(this.ToLowerBtn_Click);
            // 
            // ToUpperBtn
            // 
            this.ToUpperBtn.Location = new System.Drawing.Point(196, 174);
            this.ToUpperBtn.Name = "ToUpperBtn";
            this.ToUpperBtn.Size = new System.Drawing.Size(158, 27);
            this.ToUpperBtn.TabIndex = 4;
            this.ToUpperBtn.Text = "Zmień na duże litery";
            this.ToUpperBtn.UseVisualStyleBackColor = true;
            this.ToUpperBtn.Click += new System.EventHandler(this.ToUpperBtn_Click);
            // 
            // Wynik
            // 
            this.Wynik.AutoSize = true;
            this.Wynik.Location = new System.Drawing.Point(11, 132);
            this.Wynik.Name = "Wynik";
            this.Wynik.Size = new System.Drawing.Size(54, 17);
            this.Wynik.TabIndex = 5;
            this.Wynik.Text = "Wynik: ";
            // 
            // InputTextBox
            // 
            this.InputTextBox.Location = new System.Drawing.Point(13, 12);
            this.InputTextBox.Name = "InputTextBox";
            this.InputTextBox.Size = new System.Drawing.Size(342, 22);
            this.InputTextBox.TabIndex = 6;
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Location = new System.Drawing.Point(71, 132);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(104, 17);
            this.ResultLabel.TabIndex = 7;
            this.ResultLabel.Text = "tu będzie wynik";
            // 
            // txtSecondNr
            // 
            this.txtSecondNr.Location = new System.Drawing.Point(12, 40);
            this.txtSecondNr.Name = "txtSecondNr";
            this.txtSecondNr.Size = new System.Drawing.Size(342, 22);
            this.txtSecondNr.TabIndex = 8;
            // 
            // btnSum
            // 
            this.btnSum.Location = new System.Drawing.Point(252, 119);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(102, 30);
            this.btnSum.TabIndex = 9;
            this.btnSum.Text = "Suma";
            this.btnSum.UseVisualStyleBackColor = true;
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 327);
            this.Controls.Add(this.btnSum);
            this.Controls.Add(this.txtSecondNr);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.InputTextBox);
            this.Controls.Add(this.Wynik);
            this.Controls.Add(this.ToUpperBtn);
            this.Controls.Add(this.ToLowerBtn);
            this.Controls.Add(this.SilniaBtn);
            this.Controls.Add(this.SqrtBtn);
            this.Controls.Add(this.KwadratBtn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button KwadratBtn;
        private System.Windows.Forms.Button SqrtBtn;
        private System.Windows.Forms.Button SilniaBtn;
        private System.Windows.Forms.Button ToLowerBtn;
        private System.Windows.Forms.Button ToUpperBtn;
        private System.Windows.Forms.Label Wynik;
        private System.Windows.Forms.TextBox InputTextBox;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.TextBox txtSecondNr;
        private System.Windows.Forms.Button btnSum;
    }
}

