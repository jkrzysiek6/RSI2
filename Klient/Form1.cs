﻿using Klient.MatematykSerwisRef;
using Klient.PolonistaSerwisRef;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klient
{
    public partial class Form1 : Form
    {
        MatematykClient matematykClient = new MatematykClient();
        PolonistaClient polonistaClient = new PolonistaClient();


        public Form1()
        {
            InitializeComponent();

        }


        private void KwadratBtn_Click(object sender, EventArgs e)
        {
            try {

                ResultLabel.Text = matematykClient.Kwadrat(Convert.ToDouble(InputTextBox.Text)).ToString();
            }
            catch (Exception ex)
            {
                ResultLabel.Text = "Podaj poprawną liczbę!";
            }
        }

        private void SqrtBtn_Click(object sender, EventArgs e)
        {
            try
            {

                ResultLabel.Text = matematykClient.Pierwiastek(Convert.ToDouble(InputTextBox.Text)).ToString();
        }
            catch (Exception ex)
            {
                ResultLabel.Text = "Podaj poprawną liczbę!";
            }
}

        private void SilniaBtn_Click(object sender, EventArgs e)
        {
            try
            {

                ResultLabel.Text = matematykClient.Silnia(Convert.ToInt64(InputTextBox.Text)).ToString();
            }
            catch (Exception ex)
            {
                ResultLabel.Text = "Podaj poprawną liczbę!";
            }
        }

        private void ToLowerBtn_Click(object sender, EventArgs e)
        {
            InputTextBox.Text = polonistaClient.ToLower(InputTextBox.Text);
        }

        private void ToUpperBtn_Click(object sender, EventArgs e)
        {
            InputTextBox.Text = polonistaClient.ToUpper(InputTextBox.Text);
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            try
            {
                ResultLabel.Text = matematykClient.Suma(Convert.ToInt64(InputTextBox.Text), Convert.ToInt64(txtSecondNr.Text)).ToString();
        }
            catch (Exception ex)
            {
                ResultLabel.Text = "Podaj poprawne liczby!";
            }
}
    }
}
