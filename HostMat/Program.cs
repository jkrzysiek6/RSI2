﻿using Kontrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace HostMat
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri baseAddress = new Uri("http://localhost:10001/UslugaLiczaca");
            ServiceHost mojHost = new ServiceHost(typeof(MatematykSerwis), baseAddress);
            try
            {
                mojHost.AddServiceEndpoint(typeof(IMatematyk), new WSHttpBinding(), "MatematykSerwis");
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                mojHost.Description.Behaviors.Add(smb);
                mojHost.Open();
                Console.WriteLine("Serwis matematyka jest uruchomiony.");
                Console.WriteLine("Nacisnij <ENTER> aby zakonczyc.");
                Console.WriteLine();
                Console.ReadLine();

                mojHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("Wystapli wyjatek: {0}", ce.Message);
                mojHost.Abort();
            }
        }
    }
}
